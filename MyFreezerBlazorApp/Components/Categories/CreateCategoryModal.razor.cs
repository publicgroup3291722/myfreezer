using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Categories;

public partial class CreateCategoryModal
{
    [Parameter] public Category Category { get; set; } = new Category();

    [Parameter] public EventCallback<Category> CategoryChanged { get; set; }

    [Parameter] public EventCallback OnCategoryCreation { get; set; }

    public Modal CreateModal;

    Alert errorAlert;
    string errorMessage;

    TitleField nameField;
    TitleField descriptionField;

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        Category = new Category();
    }

    private bool IsFormValid()
    {
        if (!nameField.IsValid ||
            !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }

        return true;
    }

    private async Task CreateCategoryButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            await _requestService.PostAsJsonAsync<Category>($"/api/Category", Category);
            Category = new();
            await HideModal();
            await OnCategoryCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            //await NotificationService.Warning("Some error occured!", e.Message);
            return;
        }
    }

    public async Task ShowModal()
    {
        await CreateModal.Show();
    }

    public async Task HideModal()
    {
        await CreateModal.Hide();
    }
}
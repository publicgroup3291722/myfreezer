using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;

namespace MyFreezerBlazorApp.Components.Categories;

public partial class DeleteCategoryModal
{
    [Parameter]
    public Category category { get; set; }

    [Parameter]
    public Func<Category, Task> DeleteEntity { get; set; }
    
    public List<Group> _groupsForChange { get; set; }
    public Group _pickedGroup { get; set; } = new Group();

    Select<int> selectGroup;

    public Modal _deleteModal;

    public void ShowModal()
    {
        _deleteModal.Show();
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userId = await cookieService.GetUserId();
        _groupsForChange = await _requestService.GetFromJsonAsync<List<Group>>(
            $"/api/Group/getGroupsByUserId/{userId}"
        );
        StateHasChanged();
    }
    
    private void SelectedGroup(ChangeEventArgs e)
    {
        if (e.Value is not null)
        {
            int groupId = Convert.ToInt32(e.Value);
            if (groupId == 0)
                return;
            _pickedGroup = _groupsForChange.Where(group => group.GroupId == groupId).FirstOrDefault();
            StateHasChanged();
        }
    }
    
    private async Task YesClicked()
    {
        await _requestService.DeleteAsync(
            $"/api/Category/{category.CategoryId}" +
            $"?changerGroupId={_pickedGroup.GroupId.Value}"
        );
        await DeleteEntity.Invoke(category);
        await _deleteModal.Hide();
    }
    
    private async Task NoClicked()
    {
        await _deleteModal.Hide();
    }
}
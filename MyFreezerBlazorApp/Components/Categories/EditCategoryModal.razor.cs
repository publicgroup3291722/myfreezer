using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Categories;

public partial class EditCategoryModal
{
    [Parameter] public Category Category { get; set; } = new Category();

    [Parameter] public EventCallback<Category> CategoryChanged { get; set; }

    [Parameter] public EventCallback OnCategoryCreation { get; set; }

    public Modal EditModal;

    Alert errorAlert;
    string errorMessage;

    Select<int> selectGroup;

    TitleField nameField;
    TitleField descriptionField;

    public List<Group> _groupsForChange { get; set; }
    public Group _pickedGroup { get; set; } = new Group();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userId = await cookieService.GetUserId();
        _groupsForChange =
            await _requestService.GetFromJsonAsync<List<Group>>($"/api/Group/getGroupsByUserId/{userId}");

        StateHasChanged();
    }

    private void SelectedGroup(ChangeEventArgs e)
    {
        if (e.Value is not null)
        {
            int groupId = Convert.ToInt32(e.Value);
            if (groupId == 0)
                return;
            _pickedGroup = _groupsForChange.Where(group => group.GroupId == groupId).FirstOrDefault();
            StateHasChanged();
        }
    }

    private bool IsFormValid()
    {
        if (!nameField.IsValid ||
            !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }

        return true;
    }

    private async Task EditCategoryButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            string requestString = $"/api/Category/{Category.CategoryId}?changerGroupId={_pickedGroup.GroupId}";
            await _requestService.PutAsJsonAsync<Category>(requestString, Category);
            await HideModal();
            await OnCategoryCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
        }
    }

    public async Task ShowModal()
    {
        StateHasChanged();
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }
}
using Blazorise;
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class EmailField
{

    [Parameter]
    public string Email { get; set; }

    [Parameter]
    public EventCallback<string> EmailChanged { get; set; }

    private string emailRegex = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    
    private async Task ChangeEmail(string value)
    {
        await EmailChanged.InvokeAsync(value);
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        IsValid = Validator.Status == ValidationStatus.Success ? true : false;
    }

    Validation Validator;
    public bool IsValid { get; set; }
}
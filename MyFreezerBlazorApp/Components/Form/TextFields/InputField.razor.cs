using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class InputField
{
    [Parameter] public string Pattern { get; set; } = @"^[a-zA-ZА-Яа-я0-9_ -ЁЇїІіЄєҐґ]{3,50}$";
    [Parameter] public string Placeholder { get; set; }
    [Parameter] public string Value { get; set; }
    [Parameter] public EventCallback<string> ValueChanged { get; set; }
    private string validationError;

    private void OnInputChange(ChangeEventArgs e)
    {
        var newValue = e.Value.ToString();
        if (System.Text.RegularExpressions.Regex.IsMatch(newValue, Pattern))
        {
            validationError = null;
            Value = newValue;
            ValueChanged.InvokeAsync(newValue);
        }
        else
        {
            validationError = "Input does not match the specified pattern.";
        }
    }
}
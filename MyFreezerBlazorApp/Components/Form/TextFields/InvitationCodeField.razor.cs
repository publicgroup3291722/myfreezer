using Blazorise;
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class InvitationCodeField
{

    [Parameter]
    public string InvintationCode { get; set; }

    [Parameter]
    public EventCallback<string> InvintationCodeChanged { get; set; }

    private async Task ChangeInvintationCode(string value)
    {
        await InvintationCodeChanged.InvokeAsync(value);
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        IsValid = Validator.Status == ValidationStatus.Success ? true : false;
    }

    Validation Validator;
    public bool IsValid { get; set; }
}
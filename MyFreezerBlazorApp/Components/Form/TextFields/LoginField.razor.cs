using Blazorise;
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class LoginField
{

    [Parameter]
    public string Login { get; set; } = null;

    [Parameter]
    public EventCallback<string> LoginChanged { get; set; }

    private string LoginRegex = @"^[a-zA-Z0-9_-]{3,15}$";

    private async Task ChangeLogin(string value)
    {
        await LoginChanged.InvokeAsync(value);
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        IsValid = Validator.Status == ValidationStatus.Success ? true : false;
    }

    Validation Validator;
    public bool IsValid { get; set; }
}
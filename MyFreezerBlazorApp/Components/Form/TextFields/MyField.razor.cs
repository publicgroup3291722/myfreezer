using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class MyField
{
    [Parameter]
    public string Text { get; set; }

    [Parameter]
    public EventCallback<string> TextChanged { get; set; }

    [Parameter]
    public RenderFragment? ChildContent { get; set; }

    async void OnChange(string value)
    {
        await TextChanged.InvokeAsync(value);
    }

}

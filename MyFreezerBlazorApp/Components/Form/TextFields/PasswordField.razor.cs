using Blazorise;
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class PasswordField
{
    [Parameter]
    public string Password { get; set; }

    [Parameter]
    public EventCallback<string> PasswordChanged { get; set; }

    private string PasswordRegex = @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$";

    private async Task ChangePassword(string value)
    {
        await PasswordChanged.InvokeAsync(value);
        StateHasChanged();
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        IsValid = Validator.Status == ValidationStatus.Success ? true : false;
    }

    Validation Validator;
    public bool IsValid { get; set; }
}
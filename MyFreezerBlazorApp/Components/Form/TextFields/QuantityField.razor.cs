using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class QuantityField
{
    [Parameter]
    public int Quantity { get; set; }

    [Parameter]
    public EventCallback<int> QuantityChanged { get; set; }

    private async Task ChangeQuantity(int value)
    {
        await QuantityChanged.InvokeAsync(value);
    }
}
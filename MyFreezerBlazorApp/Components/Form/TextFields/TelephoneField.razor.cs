using Blazorise;
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class TelephoneField
{

    [Parameter]
    public string PhoneNumber { get; set; }

    [Parameter]
    public EventCallback<string> PhoneNumberChanged { get; set; }

    private async Task ChangePhoneNumber(string value)
    {
        await PhoneNumberChanged.InvokeAsync(value);
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        IsValid = Validator.Status == ValidationStatus.Success ? true : false;
    }

    Validation Validator;
    public bool IsValid { get; set; }
}
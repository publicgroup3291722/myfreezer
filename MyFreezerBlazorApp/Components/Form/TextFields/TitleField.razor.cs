﻿using Blazorise;
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Form.TextFields;

public partial class TitleField
{

    [Parameter]
    public string Title { get; set; } = null;

    [Parameter]
    public EventCallback<string> TitleChanged { get; set; }

    private string TitleRegex = @"^[a-zA-ZА-Яа-я0-9_ -ЁЇїІіЄєҐґ]{3,50}$";

    private async Task ChangeTitle(string value)
    {
        await TitleChanged.InvokeAsync(value);
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        IsValid = Validator.Status == ValidationStatus.Success ? true : false;
    }

    Validation Validator;
    public bool IsValid { get; set; }
}
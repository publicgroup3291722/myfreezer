using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Groups;

public partial class CreateGroupModal
{

    [Parameter]
    public Group Group { get; set; } = new Group();

    [Parameter]
    public EventCallback OnGroupCreation { get; set; }
    
    public Modal EditModal;

    Alert errorAlert;
    string errorMessage;

    TitleField titleField;
    TitleField descriptionField;

    private bool IsFormValid()
    {
        if (!titleField.IsValid || !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task EditGroupButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            var userId = await cookieService.GetUserId();
            var currentUser = await _requestService.GetFromJsonAsync<User>($"api/User/{userId.Value}");

            Group.Users = new List<User> { currentUser };
            await _requestService.PostAsJsonAsync("api/Group", Group);

            await HideModal();
            await OnGroupCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
            return;
        }
    }

    public async Task ShowModal()
    {
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }

}
using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Groups;

public partial class EditGroupModal
{


    [Parameter]
    public Group Group { get; set; } = new Group();

    [Parameter]
    public EventCallback OnGroupChange { get; set; }

    protected override Task OnParametersSetAsync()
    {
        _tempGroup = Group;
        return Task.CompletedTask;
    }

    public Modal EditModal;

    Alert errorAlert;
    string errorMessage;

    TitleField titleField;
    TitleField descriptionField;
    Group _tempGroup { get; set; }

    private IReadOnlyList<int> selectedUsersId { get; set; }

    private bool IsFormValid()
    {
        if (!titleField.IsValid || !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task EditGroupButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            Group = _tempGroup;

            // edit logic
            Group.Users = null;
            await _requestService.PutAsJsonAsync($"/api/Group/{Group.GroupId.Value}", Group);
            if (selectedUsersId != null)
                await _requestService.PutAsJsonAsync($"/api/Group/removeUsersFromGroup/{Group.GroupId.Value}", selectedUsersId);

            await HideModal();
            await OnGroupChange.InvokeAsync();

        }
        catch (Exception e)
        {
            await NotificationService.Warning(e.Message, "Some error occured!");
        }
    }
    
    private async Task GenerateInvintationCodeClick()
    {
        await _requestService.PutAsync($"/api/Group/generateInvintationCode/{Group.GroupId.Value}");
        await OnGroupChange.InvokeAsync();
    }
    
    private async Task ClearInvintationCodeClick()
    {
        _tempGroup.InvintationCode = null;
    }

    public async Task ShowModal()
    {
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }

}
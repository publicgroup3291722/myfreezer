using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;

namespace MyFreezerBlazorApp.Components.Groups;

public partial class GroupCard
{

    [Parameter]
    public Group Group { get; set; }

    [Parameter]
    public EventCallback OnGroupChange { get; set; }

    private void GoToGroupDetails()
    {
        navManager.NavigateTo($"group/{Group.GroupId}");
    }
    
    DeleteModal<Group> deleteModal;
    private void DeleteGroupClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeleteGroupCallback(Group group)
    {
        await _requestService.DeleteAsync($"/api/Group/{group.GroupId.Value}");
        await OnGroupChange.InvokeAsync();
    }
    
    private async Task GenerateInvintationCodeClick()
    {
        await _requestService.PutAsync($"/api/Group/generateInvintationCode/{Group.GroupId.Value}");
        //await _groupRepo.GenerateInvintationCode(Group);
        await OnGroupChange.InvokeAsync();
    }
    
    LeaveGroupModal leaveGroupModal;
    private async Task LeaveGroupClick()
    {
        await leaveGroupModal.ShowModal();
    }

    EditGroupModal editGroupModal;
    private async Task EditGroupClick()
    {
        await editGroupModal.ShowModal();
    }
}
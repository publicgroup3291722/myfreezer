using Models.Models;

namespace MyFreezerBlazorApp.Components.Groups;

public partial class GroupList
{
    private List<Group> Groups { get; set; } = null;

    JoinGroupViaCodeModal joinGroupViaCodeModal;
    CreateGroupModal createGroupModal;

    private async Task GetGroups()
    {
        var userId = await cookieService.GetUserId();
        Groups = await _requestService.GetFromJsonAsync<List<Group>>($"api/Group/getGroupsByUserId/{userId}");
        StateHasChanged();
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }
        await GetGroups();
    }

    private Group _tempGroup = new Group();

    private async Task CreateGroupButtonClicked()
    {
        await createGroupModal.ShowModal();
    }
    
    private async Task OnGroupsChange()
    {
        await GetGroups();
    }

    private async Task JoinGroupViaCode()
    {
        await joinGroupViaCodeModal.ShowModal();
    }

}
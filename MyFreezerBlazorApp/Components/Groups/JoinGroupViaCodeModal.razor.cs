using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using Models.Props;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Groups;

public partial class JoinGroupViaCodeModal
{
    [Parameter]
    public EventCallback OnGroupChange { get; set; }

    public string InvintationCode { get; set; }
    InvitationCodeField invitationCodeField;

    public Modal JoinViaCodeModal;

    Alert errorAlert;
    string errorMessage;
    
    private bool IsFormValid()
    {
        if (!invitationCodeField.IsValid)
        {
            errorMessage = "Check code!";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task JoinClick()
    {
        if (!IsFormValid())
            return;

        try
        {
            var userId = await cookieService.GetUserId();
            var group = await _requestService.GetFromJsonAsync<Group>($"api/Group/getGroupByInvintationCode/{InvintationCode}");

            if (group == null)
            {
                throw new Exception("Group is not found!");
            }
            await _requestService.PutAsJsonAsync("/api/Group/addUserToGroup",
                new GroupUserProp { UserId = userId.Value, GroupId = group.GroupId.Value }
            );

            await HideModal();
            await OnGroupChange.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning(e.Message, "Some error occured!");
        }
    }
    public async Task ShowModal()
    {
        await JoinViaCodeModal.Show();
    }

    public async Task HideModal()
    {
        await JoinViaCodeModal.Hide();
    }

}
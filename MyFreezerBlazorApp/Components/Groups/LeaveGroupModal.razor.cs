using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using Models.Props;

namespace MyFreezerBlazorApp.Components.Groups;

public partial class LeaveGroupModal
{

    [Parameter]
    public Group Group { get; set; }

    [Parameter]
    public EventCallback OnGroupChange { get; set; }

    public Modal _leaveModal;

    public async Task ShowModal()
    {
        await _leaveModal.Show();
    }

    private async Task YesClicked()
    {
        var userId = await cookieService.GetUserId();

        var prop = new GroupUserProp { UserId = userId.Value, GroupId = Group.GroupId.Value };
        await _requestService.PutAsJsonAsync("/api/Group/deleteUserFromGroup", prop);
        await _leaveModal.Hide();
        await OnGroupChange.InvokeAsync();
    }
    private async Task NoClicked()
    {
        await _leaveModal.Hide();
    }
}
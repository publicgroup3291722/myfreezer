using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Locations;

public partial class CreateLocationModal
{

    [Parameter]
    public Group Group { get; set; } = new Group();

    [Parameter]
    public Location Location { get; set; } = new Location();

    [Parameter]
    public EventCallback<Location> LocationChanged { get; set; }

    [Parameter]
    public EventCallback OnLocationCreation { get; set; }

    public Modal CreateModal;

    Alert errorAlert;
    string errorMessage;

    TitleField titleField;
    TitleField descriptionField;
    TitleField addressField;
    Group selectedGroup = new Group();

    private List<Group> _groupsForPick = new List<Group>();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        Location = new Location();
        var userId = await cookieService.GetUserId();
        var groups = await _requestService.GetFromJsonAsync<List<Group>>($"api/Group/getGroupsByUserId/{userId}");
        if (groups != null)
            _groupsForPick = groups;

        if (Group != null)
            selectedGroup = Group;
    }


    private bool IsFormValid()
    {
        if (!titleField.IsValid ||
            !descriptionField.IsValid ||
            !addressField.IsValid ||
            selectedGroup == null)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task CreateLocationButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            Location.Group = selectedGroup;
            await _requestService.PostAsJsonAsync<Location>($"/api/Location", Location);
            Location = new Location();
            await HideModal();
            await OnLocationCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
        }
    }

    public async Task ShowModal()
    {
        await CreateModal.Show();
    }

    public async Task HideModal()
    {
        await CreateModal.Hide();
    }

}
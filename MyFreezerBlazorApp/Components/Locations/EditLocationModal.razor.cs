using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Locations;

public partial class EditLocationModal
{
    [Parameter]
    public string LocationId { get; set; }

    [Parameter]
    public Location? Location { get; set; } = new Location();

    [Parameter]
    public EventCallback OnLocationChange { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        var userId = await cookieService.GetUserId();
        if (Location != null)
        {
            LocationId = Location.LocationId.ToString();
        }


        if (!int.TryParse(LocationId, out int locationIdInt))
        {
            await NotificationService.Warning("Location id must be int!");
            navManager.NavigateTo("home");
            return;
        }
        var location = await _requestService.GetFromJsonAsync<Location>($"/api/Location/{locationIdInt}");
        if (location == null)
        {
            await NotificationService.Warning("Location not found!");
            navManager.NavigateTo("home");
            return;
        }
        
        _tempLocation = location;
        StateHasChanged();
    }

    public Modal EditModal;

    Alert errorAlert;
    string errorMessage;

    TitleField titleField;
    TitleField descriptionField;
    TitleField addressField;
    Location? _tempLocation { get; set; }

    private bool IsFormValid()
    {
        if (!titleField.IsValid || !descriptionField.IsValid || !addressField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task EditLocationButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            Location = _tempLocation;

            // edit logic
            await _requestService.PutAsJsonAsync($"/api/Location/{Location.LocationId}", Location);
            _tempLocation = new Location();
            await HideModal();
            await OnLocationChange.InvokeAsync();

        }
        catch (Exception e)
        {
            await NotificationService.Warning(e.Message, "Some error occured!");
        }
    }
    public async Task ShowModal()
    {
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }
}
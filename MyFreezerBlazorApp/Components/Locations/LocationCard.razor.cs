using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;

namespace MyFreezerBlazorApp.Components.Locations;

public partial class LocationCard
{

    [Parameter]
    public Location Location { get; set; }

    [Parameter]
    public EventCallback OnLocationChange { get; set; }

    private void GoToLocationDetails()
    {
        navManager.NavigateTo($"location/{Location.LocationId}");
    }

    EditLocationModal editLocationModal;
    private async Task EditLocationClick()
    {
        await editLocationModal.ShowModal();
    }

    DeleteModal<Location> deleteModal;
    private void DeleteLocationClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeleteLocationCallback(Location location)
    {
        await _requestService.DeleteAsync($"/api/Location/{location.LocationId}");
        await OnLocationChange.InvokeAsync();
    }
}
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Services;

namespace MyFreezerBlazorApp.Components.Locations;

public partial class LocationList
{
    [Parameter]
    public Group GroupForCreation { get; set; } = new Group();

    [Parameter]
    public List<Location> Locations { get; set; } = new List<Location>();

    [Parameter]
    public EventCallback OnLocationChange { get; set; }

    CreateLocationModal createLocationModal;
    Group parentGroup { get; set; }
    
    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        await GetLocations();
    }

    private Location _tempLocation = new Location();

    private async Task CreateLocationButtonClicked()
    {
        if (GroupForCreation.GroupId == null)
            throw new ArgumentNullException("GroupId of GroupForCreation is null");
        _tempLocation.GroupId = GroupForCreation.GroupId.Value;
        _tempLocation.Group = GroupForCreation;
        await createLocationModal.ShowModal();
    }
    private async Task GetLocations()
    {
        var groupId = GroupForCreation.GroupId.Value;
        var locations = await _requestService
        .GetFromJsonAsync<List<Location>>($"/api/location/getLocationsByGroupId/{groupId}");
        Locations = locations.ToList();
        StateHasChanged();
    }
}
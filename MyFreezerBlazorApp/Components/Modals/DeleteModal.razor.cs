using Blazorise;
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components.Modals;

public partial class DeleteModal<TItem>
{

    [Parameter]
    public string EntityName { get; set; }

    [Parameter]
    public TItem Entity { get; set; }

    [Parameter]
    public Func<TItem, Task> DeleteEntity { get; set; }

    public Modal _deleteModal;

    public void ShowModal()
    {
        _deleteModal.Show();
    }

    private async Task YesClicked()
    {
        await DeleteEntity.Invoke(Entity);
        await _deleteModal.Hide();
    }
    
    private async Task NoClicked()
    {
        await _deleteModal.Hide();
    }
}
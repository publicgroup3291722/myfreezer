using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Places;

public partial class CreatePlaceModal
{
    [Parameter]
    public Location Location { get; set; } = new Location();

    [Parameter]
    public Place Place { get; set; } = new Place();

    [Parameter]
    public EventCallback<Place> PlaceChanged { get; set; }

    [Parameter]
    public EventCallback OnPlaceCreation { get; set; }

    public Modal CreateModal;

    Alert errorAlert;
    string errorMessage;

    TitleField nameField;
    TitleField descriptionField;
    int? selectedLocationId { get; set; }
    Location selectedLocation { get; set; }

    private List<Location> _locationsForPick = new List<Location>();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        Place = new Place();
        var userId = await cookieService.GetUserId();
        var locations = await _requestService.GetFromJsonAsync<List<Location>>($"api/Location/getLocationsByUserId/{userId}");
        if (locations != null)
            _locationsForPick = locations;

        if (Location != null)
            selectedLocation = Location;
    }
    
    private bool IsFormValid()
    {
        if (!nameField.IsValid ||
            !descriptionField.IsValid ||
            selectedLocation == null)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task CreatePlaceButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            Place.Location = selectedLocation;
            await _requestService.PostAsJsonAsync<Place>($"/api/Place", Place);
            await HideModal();
            await OnPlaceCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
        }
    }

    public async Task ShowModal()
    {
        await CreateModal.Show();
    }

    public async Task HideModal()
    {
        await CreateModal.Hide();
    }

}
using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.Places;

public partial class EditPlaceModal
{
    [Parameter]
    public Place Place { get; set; } = new Place();

    [Parameter]
    public EventCallback OnPlaceChange { get; set; }

    protected override Task OnParametersSetAsync()
    {
        _tempPlace = Place;
        return Task.CompletedTask;
    }

    public Modal EditModal;

    Alert errorAlert;
    string errorMessage;

    TitleField nameField;
    TitleField descriptionField;
    Place _tempPlace { get; set; }
    
    private bool IsFormValid()
    {
        if (!nameField.IsValid || !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task EditPlaceButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            Place = _tempPlace;

            // edit logic
            await _requestService.PutAsJsonAsync($"/api/Place/{Place.PlaceId}", Place);
            await HideModal();
            await OnPlaceChange.InvokeAsync();

        }
        catch (Exception e)
        {
            await NotificationService.Warning(e.Message, "Some error occured!");
        }
    }
    public async Task ShowModal()
    {
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }

}
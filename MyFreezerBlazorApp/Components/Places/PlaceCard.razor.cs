using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;

namespace MyFreezerBlazorApp.Components.Places;

public partial class PlaceCard
{

    [Parameter]
    public Place Place { get; set; }

    [Parameter]
    public EventCallback OnPlaceChange { get; set; }

    private void GoToPlaceDetails()
    {
        navManager.NavigateTo($"place/{Place.PlaceId}");
    }

    EditPlaceModal editPlaceModal;
    private async Task EditPlaceClick()
    {
        await editPlaceModal.ShowModal();
    }

    DeleteModal<Place> deleteModal;
    private void DeletePlaceClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeletePlaceCallback(Place place)
    {
        await _requestService.DeleteAsync($"/api/Place/{place.PlaceId}");
        await OnPlaceChange.InvokeAsync();
    }
}
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Services;

namespace MyFreezerBlazorApp.Components.Places;

public partial class PlaceList
{
    [Parameter]
    public Location LocationForCreation { get; set; } = new Location();

    [Parameter]
    public List<Place> Places { get; set; } = new List<Place>();

    [Parameter]
    public EventCallback OnPlaceChange { get; set; }

    CreatePlaceModal createPlaceModal;
    Location parentLocation { get; set; }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        await GetPlaces();
    }

    private Place _tempPlace = new Place();

    private async Task CreatePlaceButtonClicked()
    {
        if (LocationForCreation.LocationId == null)
            throw new ArgumentNullException("LocationId of LocationForCreation is null");
        _tempPlace.LocationId = LocationForCreation.LocationId;
        _tempPlace.Location = LocationForCreation;
        await createPlaceModal.ShowModal();
    }
    private async Task GetPlaces()
    {
        var locationId = LocationForCreation.LocationId;
        var locations = await _requestService
        .GetFromJsonAsync<List<Place>>($"/api/Place/getPlacesByLocationId/{locationId}");
        Places = locations.ToList();
        StateHasChanged();
    }
}
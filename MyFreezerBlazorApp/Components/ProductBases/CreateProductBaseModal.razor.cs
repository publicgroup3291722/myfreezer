using Blazorise;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Models.Enums;
using Models.Models;
using Models.Options;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.ProductBases;

public partial class CreateProductBaseModal
{
    [Parameter]
    public ProductBase ProductBase { get; set; } = new ProductBase();

    [Parameter]
    public EventCallback<ProductBase> ProductBaseChanged { get; set; }

    [Parameter]
    public EventCallback OnProductBaseCreation { get; set; }

    public Modal CreateModal;

    Alert errorAlert;
    string errorMessage;

    Select<string> select;

    TitleField nameField;
    TitleField descriptionField;

    public string categiesSearch { get; set; } = "";

    public List<Category> _categoriesForPick = new List<Category>();
    public List<Category> _pickedCategories = new List<Category>();
    public Category _pickedCategory { get; set; } = new Category();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        ProductBase = new ProductBase();
        var userId = await cookieService.GetUserId();

        await CategorySearchChanged("");
    }
    private void SelectedCategories(ChangeEventArgs e)
    {
        if (e.Value is not null)
        {
            string title = e.Value as string;
            if (title == "")
                return;
            _pickedCategories.Add(_categoriesForPick.Where(c => c.Title == title).First());
            foreach (var pickedCategory in _pickedCategories)
            {
                _categoriesForPick.Remove(_categoriesForPick.Where(c => c.CategoryId == pickedCategory.CategoryId).FirstOrDefault());
            }
            select.SelectedValue = "";
            select.Revalidate();
            StateHasChanged();
        }
    }
    private void OnDeleteCategory(MouseEventArgs e)
    {
        int CategoryId = (int)(e.Detail);
        _pickedCategories.Remove(_pickedCategories.Where(c => c.CategoryId == CategoryId).First());
    }

    public async Task CategorySearchChanged(string value)
    {
        categiesSearch = value;
        var options = new CategorySortFilterPageOptions
        {
            OrderBy = CategoryOrderBy.ByCategoryIdASC,
            FilterBy = CategoryFilterBy.ByTitle,
            FilterValue = categiesSearch,
            PageNum = 50,
            PageStart = 0
        };
        string query = await _requestService.GetRequestStringWithFormedOptions("api/Category", options);
        _categoriesForPick = await _requestService.GetFromJsonAsync<List<Category>>(query);
        foreach (var pickedCategory in _pickedCategories)
        {
            _categoriesForPick.Remove(_categoriesForPick.Where(c => c.CategoryId == pickedCategory.CategoryId).First());
        }
        StateHasChanged();
    }

    private bool IsFormValid()
    {
        if (!nameField.IsValid ||
            !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task CreateProductBaseButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            ProductBase.Categories = _pickedCategories;
            await _requestService.PostAsJsonAsync<ProductBase>($"/api/ProductBase", ProductBase);
            _pickedCategories.Clear();
            await CategorySearchChanged("");
            await HideModal();
            await OnProductBaseCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
        }
    }

    public async Task ShowModal()
    {
        await CreateModal.Show();
    }

    public async Task HideModal()
    {
        await CreateModal.Hide();
    }

}
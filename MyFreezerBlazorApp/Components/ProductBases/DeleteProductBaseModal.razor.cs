using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;

namespace MyFreezerBlazorApp.Components.ProductBases;

public partial class DeleteProductBaseModal
{
    [Parameter] public ProductBase productBase { get; set; }

    [Parameter] public Func<ProductBase, Task> DeleteEntity { get; set; }

    public List<Group> _groupsForChange { get; set; }
    public Group _pickedGroup { get; set; } = new Group();

    Select<int> selectGroup;

    public Modal _deleteModal;

    public void ShowModal()
    {
        _deleteModal.Show();
    }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userId = await cookieService.GetUserId();
        _groupsForChange = await _requestService.GetFromJsonAsync<List<Group>>(
            $"/api/Group/getGroupsByUserId/{userId}"
        );
        StateHasChanged();
    }

    private void SelectedGroup(ChangeEventArgs e)
    {
        if (e.Value is not null)
        {
            int groupId = Convert.ToInt32(e.Value);
            if (groupId == 0)
                return;
            _pickedGroup = _groupsForChange.Where(group => group.GroupId == groupId).FirstOrDefault();
            StateHasChanged();
        }
    }

    private async Task YesClicked()
    {
        if (_pickedGroup != null && _pickedGroup.GroupId != null)
        {
            await _requestService.DeleteAsync(
                $"/api/ProductBase/{productBase.ProductBaseId}" +
                $"?changerGroupId={_pickedGroup.GroupId.Value}"
            );
            await DeleteEntity.Invoke(productBase);
            await _deleteModal.Hide();
        }
    }

    private async Task NoClicked()
    {
        await _deleteModal.Hide();
    }
}
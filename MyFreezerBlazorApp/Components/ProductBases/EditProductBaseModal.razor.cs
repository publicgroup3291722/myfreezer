using Blazorise;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Models.Enums;
using Models.Models;
using Models.Options;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.ProductBases;

public partial class EditProductBaseModal
{
    [Parameter] public ProductBase ProductBase { get; set; } = new ProductBase();

    [Parameter] public EventCallback<ProductBase> ProductBaseChanged { get; set; }

    [Parameter] public EventCallback OnProductBaseCreation { get; set; }

    public Modal EditModal;

    Alert errorAlert;
    string errorMessage;

    Select<int> selectCategory;
    Select<int> selectGroup;

    TitleField nameField;
    TitleField descriptionField;

    public string categiesSearch { get; set; } = "";

    public List<Category> _categoriesForPick = new List<Category>();
    public List<Category> _pickedCategories = new List<Category>();
    public Category _pickedCategory { get; set; } = new Category();

    public List<Group> _groupsForChange { get; set; }
    public Group _pickedGroup { get; set; } = new Group();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userId = await cookieService.GetUserId();
        _groupsForChange =
            await _requestService.GetFromJsonAsync<List<Group>>($"/api/Group/getGroupsByUserId/{userId}");
        _pickedCategories = ProductBase.Categories;
        await CategorySearchChanged("");
        StateHasChanged();
    }

    private void SelectedCategories(ChangeEventArgs e)
    {
        if (e.Value is null)
            return;
        
        int categoryId = Convert.ToInt32(e.Value);
        if (categoryId == 0)
            return;
        _pickedCategories.Add(_categoriesForPick.Where(c => c.CategoryId == categoryId).First());
        foreach (var pickedCategory in _pickedCategories)
        {
            _categoriesForPick.Remove(_categoriesForPick.Where(c => c.CategoryId == pickedCategory.CategoryId)
                .FirstOrDefault());
        }

        StateHasChanged();
    }

    private void OnDeleteCategory(MouseEventArgs e)
    {
        int CategoryId = (int)(e.Detail);
        _pickedCategories.Remove(_pickedCategories.Where(c => c.CategoryId == CategoryId).First());
    }

    public async Task CategorySearchChanged(string value)
    {
        categiesSearch = value;
        var options = new CategorySortFilterPageOptions
        {
            OrderBy = CategoryOrderBy.ByCategoryIdASC,
            FilterBy = CategoryFilterBy.ByTitle,
            FilterValue = categiesSearch,
            PageNum = 50,
            PageStart = 0
        };
        string query = await _requestService.GetRequestStringWithFormedOptions("api/Category", options);
        _categoriesForPick = await _requestService.GetFromJsonAsync<List<Category>>(query);

        if (_pickedCategories != null)
        {
            foreach (var pickedCategory in _pickedCategories)
            {
                _categoriesForPick.Remove(_categoriesForPick.Where(c => c.CategoryId == pickedCategory.CategoryId)
                    .First());
            }
        }

        StateHasChanged();
    }

    private void SelectedGroup(ChangeEventArgs e)
    {
        if (e.Value is not null)
        {
            int groupId = Convert.ToInt32(e.Value);
            if (groupId == 0)
                return;
            _pickedGroup = _groupsForChange.Where(group => group.GroupId == groupId).FirstOrDefault();
            StateHasChanged();
        }
    }

    private bool IsFormValid()
    {
        if (!nameField.IsValid ||
            !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }

        return true;
    }

    private async Task EditProductBaseButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            ProductBase.Categories = _pickedCategories;
            string requestString =
                $"/api/ProductBase/{ProductBase.ProductBaseId}?changerGroupId={_pickedGroup.GroupId}";
            await _requestService.PutAsJsonAsync<ProductBase>(requestString, ProductBase);
            await HideModal();
            await OnProductBaseCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
        }
    }

    public async Task ShowModal()
    {
        StateHasChanged();
        _pickedCategories = ProductBase.Categories;
        await CategorySearchChanged("");
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }
}
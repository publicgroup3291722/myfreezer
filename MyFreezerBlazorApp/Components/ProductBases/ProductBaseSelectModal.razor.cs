using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Enums;
using Models.Models;
using Models.Options;

namespace MyFreezerBlazorApp.Components.ProductBases;

public partial class ProductBaseSelectModal
{
    string searchLine { get; set; }

    List<ProductBase> searchedPB { get; set; }

    [Parameter]
    public ProductBase selectedProductBase { get; set; }

    [Parameter]
    public EventCallback<ProductBase> selectedProductBaseChanged { get; set; }
    
    private async Task SearchProductBase()
    {
        var options = new ProductBaseSortFilterPageOptions()
        {
            OrderBy = ProductBaseOrderBy.ByProductBaseIdDESC,
            FilterBy = ProductBaseFilterBy.ByName,
            FilterValue = searchLine,
            PageNum = 20,
            PageStart = 0
        };
        string queryString = await _requestService.GetRequestStringWithFormedOptions($"/api/ProductBase", options);
        var productBases = await _requestService.GetFromJsonAsync<List<ProductBase>>(queryString);
        searchedPB = productBases;
        StateHasChanged();
    }

    private async Task ProductBaseSelected()
    {
        await selectedProductBaseChanged.InvokeAsync(selectedProductBase);
        await HideModal();
    }

    Modal modal;
    public async Task ShowModal()
    {
        await modal.Show();
    }

    public async Task HideModal()
    {
        await modal.Hide();
    }
}
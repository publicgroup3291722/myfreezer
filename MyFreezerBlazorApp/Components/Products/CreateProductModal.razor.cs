using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;
using MyFreezerBlazorApp.Components.ProductBases;

namespace MyFreezerBlazorApp.Components.Products;

public partial class CreateProductModal
{
    [Parameter] public Place Place { get; set; } = new Place();

    [Parameter] public string LocationId { get; set; }

    [Parameter] public Product Product { get; set; } = new Product();

    [Parameter] public EventCallback<Product> ProductChanged { get; set; }

    [Parameter] public EventCallback OnProductCreation { get; set; }

    public Modal CreateModal;
    ProductBaseSelectModal productBaseSelectModal;

    Alert errorAlert;
    string errorMessage;

    TitleField nameField;
    TitleField descriptionField;
    int? selectedPlaceId { get; set; }
    Place selectedPlace { get; set; }

    ProductBase selectedProductBase { get; set; }

    private List<Place> _placesForPick = new List<Place>();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        Product = new Product();
        var userId = await cookieService.GetUserId();
        var places = await _requestService.GetFromJsonAsync<List<Place>>(
            $"api/Place/getPlacesByLocationId/{Place.LocationId}"
        );
        if (places != null)
            _placesForPick = places;

        if (Place != null)
            selectedPlace = Place;
    }


    private bool IsFormValid()
    {
        if (selectedPlace == null || selectedProductBase == null)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }

        return true;
    }

    private async Task CreateProductButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            Product.Place = selectedPlace;
            Product.ProductBase = selectedProductBase;
            await _requestService.PostAsJsonAsync<Product>($"/api/Product", Product);
            await HideModal();
            await OnProductCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
            return;
        }
    }

    public async Task ShowModal()
    {
        await CreateModal.Show();
    }

    public async Task HideModal()
    {
        await CreateModal.Hide();
    }
}
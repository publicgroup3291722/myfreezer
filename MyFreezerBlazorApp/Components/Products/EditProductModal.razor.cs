using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.ProductBases;

namespace MyFreezerBlazorApp.Components.Products;

public partial class EditProductModal
{
    [Parameter]
    public Product Product { get; set; } = new Product();

    [Parameter]
    public EventCallback OnProductChange { get; set; }

    protected override Task OnParametersSetAsync()
    {
        _tempProduct = Product;
        return Task.CompletedTask;
    }

    public Modal EditModal;
    ProductBaseSelectModal productBaseSelectModal;

    Alert errorAlert;
    string errorMessage;
    
    Product _tempProduct { get; set; }

    ProductBase selectedProductBase { get; set; }

    private bool IsFormValid()
    {
        if (_tempProduct.Quantity < 1 || _tempProduct.Price < 0)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task EditProductButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            Product = _tempProduct;

            // edit logic
            await _requestService.PutAsJsonAsync($"/api/Product/{Product.ProductId}", Product);
            await HideModal();
            await OnProductChange.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning(e.Message, "Some error occured!");
        }
    }
    public async Task ShowModal()
    {
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }

}
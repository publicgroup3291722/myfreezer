using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;

namespace MyFreezerBlazorApp.Components.Products;

public partial class ProductCard
{
    [Parameter]
    public Product Product { get; set; }

    [Parameter]
    public EventCallback OnProductChange { get; set; }
    
    EditProductModal editProductModal;
    private async Task EditProductClick()
    {
        await editProductModal.ShowModal();
    }

    DeleteModal<Product> deleteModal;
    private void DeleteProductClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeleteProductCallback(Product product)
    {
        await _requestService.DeleteAsync($"/api/Product/{product.ProductId}");
        await OnProductChange.InvokeAsync();
    }
}
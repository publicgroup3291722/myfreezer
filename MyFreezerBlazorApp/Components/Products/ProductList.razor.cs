using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Services;

namespace MyFreezerBlazorApp.Components.Products;

public partial class ProductList
{
    [Parameter]
    public Place PlaceForCreation { get; set; } = new Place();

    [Parameter]
    public List<Product> Products { get; set; } = new List<Product>();

    [Parameter]
    public EventCallback OnProductChange { get; set; }

    CreateProductModal createProductModal;
    Place parentPlace { get; set; }
    
    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        await GetProducts();
    }

    private Product _tempProduct = new Product();

    private async Task CreateProductButtonClicked()
    {
        if (PlaceForCreation.PlaceId == null)
            throw new ArgumentNullException("PlaceId of PlaceForCreation is null");
        _tempProduct.PlaceId = PlaceForCreation.PlaceId;
        _tempProduct.Place = PlaceForCreation;
        await createProductModal.ShowModal();
    }
    private async Task GetProducts()
    {
        var placeId = PlaceForCreation.PlaceId;
        var products = await _requestService
        .GetFromJsonAsync<List<Product>>($"/api/Product/getProductsByPlaceId/{placeId}");
        Products = products.ToList();
        StateHasChanged();
    }
}
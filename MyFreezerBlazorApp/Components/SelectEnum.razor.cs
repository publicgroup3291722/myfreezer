using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components;

public partial class SelectEnum<TItem>
{
    [Parameter]
    public string LabelContent { get; set; } = "";

    [Parameter]
    public TItem selectedItem { get; set; }

    [Parameter]
    public EventCallback<TItem> selectedItemChanged { get; set; }

    private List<TItem> allValues { get; set; }

    protected override Task OnInitializedAsync()
    {
        var t = Enum.GetValues(typeof(TItem)).Cast<TItem>().ToList();
        allValues = t;
        return Task.CompletedTask;
    }

    private async Task SelectedValueChanged(TItem value)
    {
        selectedItem = value;
        await selectedItemChanged.InvokeAsync(value);
    }
}
using Microsoft.AspNetCore.Components;

namespace MyFreezerBlazorApp.Components;

public partial class SelectPage
{
    [Parameter]
    public string LabelContent { get; set; } = "";

    [Parameter]
    public int selectedItem { get; set; }

    [Parameter]
    public EventCallback<int> selectedItemChanged { get; set; }

    [Parameter]
    public int MaxValue { get; set; }

    private List<int> allValues { get; set; }

    [Parameter]
    public EventCallback OnPageChanged { get; set; }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        allValues = new List<int>();
        for (int i = 1; i <= MaxValue; i++)
            allValues.Add(i);
    }
    
    private async Task SelectedValueChanged(int value)
    {
        selectedItem = value;
        await selectedItemChanged.InvokeAsync(value);
        await OnPageChanged.InvokeAsync();
    }
    
    public async Task Refresh()
    {
        StateHasChanged();
    }
}
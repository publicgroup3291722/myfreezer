using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.ShoppingLists;

public partial class CreateShoppingListModal
{
    [Parameter]
    public Group Group { get; set; } = new Group();

    [Parameter]
    public ShoppingList ShoppingList { get; set; } = new ShoppingList();

    [Parameter]
    public EventCallback<ShoppingList> ShoppingListChanged { get; set; }

    [Parameter]
    public EventCallback OnShoppingListCreation { get; set; }

    public Modal CreateModal;

    Alert errorAlert;
    string errorMessage;

    TitleField titleField;
    TitleField descriptionField;
    Group selectedGroup = new Group();

    private List<Group> _groupsForPick = new List<Group>();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        ShoppingList = new ShoppingList();
        var userId = await cookieService.GetUserId();
        var groups = await _requestService.GetFromJsonAsync<List<Group>>($"api/Group/getGroupsByUserId/{userId}");
        if (groups != null)
            _groupsForPick = groups;

        if (Group != null)
            selectedGroup = Group;
    }
    
    private bool IsFormValid()
    {
        if (!titleField.IsValid ||
            !descriptionField.IsValid ||
            selectedGroup == null)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task CreateShoppingListButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            ShoppingList.Group = selectedGroup;
            await _requestService.PostAsJsonAsync<ShoppingList>($"/api/ShoppingList", ShoppingList);
            ShoppingList = new ShoppingList();
            await HideModal();
            await OnShoppingListCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
        }
    }

    public async Task ShowModal()
    {
        await CreateModal.Show();
    }

    public async Task HideModal()
    {
        await CreateModal.Hide();
    }

}
using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Components.ShoppingLists;

public partial class EditShoppingListModal
{
    [Parameter]
    public ShoppingList ShoppingList { get; set; } = new ShoppingList();

    [Parameter]
    public EventCallback OnShoppingListChange { get; set; }

    protected override Task OnParametersSetAsync()
    {
        _tempShoppingList = ShoppingList;
        return Task.CompletedTask;
    }

    public Modal EditModal;

    Alert errorAlert;
    string errorMessage;

    TitleField titleField;
    TitleField descriptionField;
    TitleField addressField;
    ShoppingList _tempShoppingList { get; set; }
    
    private bool IsFormValid()
    {
        if (!titleField.IsValid || !descriptionField.IsValid)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task EditShoppingListButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            ShoppingList = _tempShoppingList;

            // edit logic
            await _requestService.PutAsJsonAsync($"/api/ShoppingList/{ShoppingList.ShoppingListId}", ShoppingList);
            _tempShoppingList = new ShoppingList();
            await HideModal();
            await OnShoppingListChange.InvokeAsync();

        }
        catch (Exception e)
        {
            await NotificationService.Warning(e.Message, "Some error occured!");
        }
    }
    public async Task ShowModal()
    {
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }

}
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;

namespace MyFreezerBlazorApp.Components.ShoppingLists;

public partial class ShoppingListCard
{
    [Parameter]
    public ShoppingList ShoppingList { get; set; }

    [Parameter]
    public EventCallback OnShoppingListChange { get; set; }

    private void GoToShoppingListDetails()
    {
        navManager.NavigateTo($"shoppingList/{ShoppingList.ShoppingListId}");
    }

    EditShoppingListModal editShoppingListModal;
    private async Task EditShoppingListClick()
    {
        await editShoppingListModal.ShowModal();
    }

    DeleteModal<ShoppingList> deleteModal;
    private void DeleteShoppingListClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeleteShoppingListCallback(ShoppingList shoppingList)
    {
        await _requestService.DeleteAsync($"/api/ShoppingList/{shoppingList.ShoppingListId}");
        await OnShoppingListChange.InvokeAsync();
    }
}
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Services;

namespace MyFreezerBlazorApp.Components.ShoppingLists;

public partial class ShoppingListList
{
    [Parameter]
    public Group GroupForCreation { get; set; } = new Group();

    [Parameter]
    public List<ShoppingList> ShoppingLists { get; set; } = new List<ShoppingList>();

    [Parameter]
    public EventCallback OnShoppingListChange { get; set; }

    CreateShoppingListModal createShoppingListModal;
    Group parentGroup { get; set; }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        await GetShoppingLists();
    }

    private ShoppingList _tempShoppingList = new ShoppingList();

    private async Task CreateShoppingListButtonClicked()
    {
        if (GroupForCreation.GroupId == null)
            throw new ArgumentNullException("GroupId of GroupForCreation is null");
        _tempShoppingList.GroupId = GroupForCreation.GroupId.Value;
        _tempShoppingList.Group = GroupForCreation;
        await createShoppingListModal.ShowModal();
    }
    private async Task GetShoppingLists()
    {
        var groupId = GroupForCreation.GroupId.Value;
        var shoppingLists = await _requestService
        .GetFromJsonAsync<List<ShoppingList>>($"/api/shoppingList/getShoppingListsByGroupId/{groupId}");
        ShoppingLists = shoppingLists.ToList();
        StateHasChanged();
    }
}
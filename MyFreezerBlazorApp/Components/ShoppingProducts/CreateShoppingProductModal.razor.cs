using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Form.TextFields;
using MyFreezerBlazorApp.Components.ProductBases;

namespace MyFreezerBlazorApp.Components.ShoppingProducts;

public partial class CreateShoppingProductModal
{

    [Parameter]
    public ShoppingList ShoppingList { get; set; }

    [Parameter]
    public ShoppingProduct ShoppingProduct { get; set; } = new ShoppingProduct();

    [Parameter]
    public EventCallback<ShoppingProduct> ShoppingProductChanged { get; set; }

    [Parameter]
    public EventCallback OnShoppingProductCreation { get; set; }

    public Modal CreateModal;

    Alert errorAlert;
    string errorMessage;

    TitleField nameField;
    TitleField descriptionField;
    int? selectedShoppingListId { get; set; }
    ShoppingList selectedShoppingList { get; set; }

    ProductBase selectedProductBase { get; set; }

    private List<ShoppingList> _shoppingListsForPick = new List<ShoppingList>();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        selectedShoppingList = ShoppingList;
        if (!firstRender)
            return;

        var userId = await cookieService.GetUserId();
        if (ShoppingList.GroupId == null)
            return;
        var shoppingLists = await _requestService.GetFromJsonAsync<List<ShoppingList>>(
            $"api/ShoppingList/getShoppingListsByGroupId/{ShoppingList.GroupId}"
        );
        if (shoppingLists != null)
            _shoppingListsForPick = shoppingLists;

        if (ShoppingList != null)
            selectedShoppingList = ShoppingList;
    }

    private bool IsFormValid()
    {
        if (selectedShoppingList == null || selectedProductBase == null || ShoppingProduct.Quantity < 1)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task CreateProductButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            ShoppingProduct.ShoppingList = selectedShoppingList;
            ShoppingProduct.ProductBase = selectedProductBase;
            await _requestService.PostAsJsonAsync<ShoppingProduct>($"/api/ShoppingProduct", ShoppingProduct);
            await HideModal();
            await OnShoppingProductCreation.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning("Some error occured!", e.Message);
        }
    }

    public async Task ShowModal()
    {
        await CreateModal.Show();
    }

    public async Task HideModal()
    {
        await CreateModal.Hide();
    }



    ProductBaseSelectModal productBaseSelectModal;

}
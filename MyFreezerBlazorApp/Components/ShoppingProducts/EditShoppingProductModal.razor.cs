using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.ProductBases;

namespace MyFreezerBlazorApp.Components.ShoppingProducts;

public partial class EditShoppingProductModal
{
    [Parameter]
    public ShoppingProduct ShoppingProduct { get; set; } = new ShoppingProduct();

    [Parameter]
    public EventCallback OnProductChange { get; set; }

    protected override Task OnParametersSetAsync()
    {
        _tempShoppingProduct = ShoppingProduct;
        return Task.CompletedTask;
    }

    public Modal EditModal;
    ProductBaseSelectModal productBaseSelectModal;

    Alert errorAlert;
    string errorMessage;
    
    ShoppingProduct _tempShoppingProduct { get; set; }

    ProductBase selectedProductBase { get; set; }
    
    private bool IsFormValid()
    {
        if (_tempShoppingProduct.Quantity < 1 || selectedProductBase == null)
        {
            errorMessage = "Check fields values";
            errorAlert.Visible = true;
            return false;
        }
        return true;
    }

    private async Task EditProductButtonClicked()
    {
        if (!IsFormValid())
            return;

        try
        {
            ShoppingProduct = _tempShoppingProduct;

            // edit logic
            await _requestService.PutAsJsonAsync($"/api/Product/{ShoppingProduct.ShoppingProductId}", _tempShoppingProduct);
            await HideModal();
            await OnProductChange.InvokeAsync();
        }
        catch (Exception e)
        {
            await NotificationService.Warning(e.Message, "Some error occured!");
        }
    }
    public async Task ShowModal()
    {
        await EditModal.Show();
    }

    public async Task HideModal()
    {
        await EditModal.Hide();
    }

}
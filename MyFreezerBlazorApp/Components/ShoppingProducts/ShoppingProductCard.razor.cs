using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;
using MyFreezerBlazorApp.Components.Products;

namespace MyFreezerBlazorApp.Components.ShoppingProducts;

public partial class ShoppingProductCard
{

    [Parameter]
    public ShoppingProduct ShoppingProduct { get; set; }

    [Parameter]
    public EventCallback OnShoppingProductChange { get; set; }
    
    EditProductModal editProductModal;
    private async Task EditProductClick()
    {
        await editProductModal.ShowModal();
    }

    DeleteModal<ShoppingProduct> deleteModal;
    private void DeleteProductClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeleteShoppingProductCallback(ShoppingProduct shoppingProduct)
    {
        await _requestService.DeleteAsync($"/api/Product/{shoppingProduct.ShoppingProductId}");
        await OnShoppingProductChange.InvokeAsync();
    }
}
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Services;

namespace MyFreezerBlazorApp.Components.ShoppingProducts;

public partial class ShoppingProductList
{
    [Parameter]
    public ShoppingList ShoppingListForCreation { get; set; } = new ShoppingList();

    [Parameter]
    public List<ShoppingProduct> ShoppingProducts { get; set; } = new List<ShoppingProduct>();

    [Parameter]
    public EventCallback OnShoppingProductChange { get; set; }

    CreateShoppingProductModal createShoppingProductModal;
    ShoppingList parentShoppingList { get; set; }

    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        await GetShoppingProducts();
    }

    private ShoppingProduct _tempShoppingProduct = new ShoppingProduct();

    private async Task CreateShoppingProductButtonClicked()
    {
        if (ShoppingListForCreation.ShoppingListId == null)
            throw new ArgumentNullException("ShoppingListId of ShoppingListForCreation is null");
        _tempShoppingProduct.ShoppingListId = ShoppingListForCreation.ShoppingListId;
        _tempShoppingProduct.ShoppingList = ShoppingListForCreation;
        await createShoppingProductModal.ShowModal();
    }
    private async Task GetShoppingProducts()
    {
        var shoppingListId = ShoppingListForCreation.ShoppingListId;
        //var shoppingLists = await _shoppingListRepo.GetShoppingProductsByShoppingListId(ShoppingListForCreation.ShoppingListId.Value);
        var shoppingProducts = await _requestService
        .GetFromJsonAsync<List<ShoppingProduct>>($"/api/ShoppingProduct/getShoppingProductsByShoppingListId/{shoppingListId}");
        ShoppingProducts = shoppingProducts.ToList();
        StateHasChanged();
    }
}
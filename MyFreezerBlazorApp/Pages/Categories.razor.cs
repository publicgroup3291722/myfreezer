using Models.Enums;
using Models.Models;
using Models.Options;
using MyFreezerBlazorApp.Components;
using MyFreezerBlazorApp.Components.Categories;

namespace MyFreezerBlazorApp.Pages;

public partial class Categories
{

    public int pageStart { get; set; } = 1;
    public int pageNum { get; set; } = 10;
    public int pageItems { get; set; }

    private CategoryOrderBy orderBy;
    private CategoryFilterBy filterBy;
    private string filterValue { get; set; }

    private List<Category> _searchedCategorys { get; set; }

    Category _tempCategory { get; set; } = new Category();

    CreateCategoryModal createCategoryModal;
    EditCategoryModal editCategoryModal;

    SelectPage selectPage;
    SelectPageNum selectPageNum;
    DeleteCategoryModal deleteModal;

    private Task CreateCategoryClick()
    {
        createCategoryModal.ShowModal();
        return Task.CompletedTask;
    }
    
    private Task EditCategoryClick()
    {
        editCategoryModal.Category = _tempCategory;
        editCategoryModal.ShowModal();
        return Task.CompletedTask;
    }
    
    private async Task DeleteCategoryClick()
    {
        deleteModal.ShowModal();
    }
    
    private async Task OnCategoryDelete(Category category)
    {
        await LoadCategories();
    }
    
    private async Task SearchClick()
    {
        await LoadCategories();
        _tempCategory = new();
    }
    
    private async Task LoadCategories()
    {
        var options = new CategorySortFilterPageOptions()
        {
            OrderBy = orderBy,
            FilterBy = filterBy,
            FilterValue = filterValue,
            PageNum = pageNum,
            PageStart = pageStart - 1
        };
        string query = await _requestService.GetRequestStringWithFormedOptions("api/Category/getCountByOptions", options);
        int count = await _requestService.GetFromJsonAsync<int>(query);
        var baseValue = ((double)count / pageNum);
        pageItems = Convert.ToInt32(baseValue);
        if (baseValue - pageItems > 0)
            pageItems++;
        selectPage.MaxValue = pageItems;
        await selectPage.Refresh();

        var requestString = await _requestService.GetRequestStringWithFormedOptions("api/Category", options);
        var categorys = await _requestService.GetFromJsonAsync<List<Category>>(requestString);
        _searchedCategorys = categorys;
        StateHasChanged();
    }
    
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        await LoadCategories();
    }
}
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Groups;
using MyFreezerBlazorApp.Components.Modals;

namespace MyFreezerBlazorApp.Pages;

public partial class GroupDetails
{
    [Parameter]
    public string groupId { get; set; }

    public Group CurrentGroup { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        if (!int.TryParse(groupId, out int groupIdInt))
        {
            await NotificationService.Warning("Group id must be int!");
            navManager.NavigateTo("home");
            return;
        }
        var group = await _requestService.GetFromJsonAsync<Group>($"/api/Group/{groupIdInt}");

        if (group == null)
        {
            await NotificationService.Warning("Group not found!");
            navManager.NavigateTo("home");
            return;
        }
        CurrentGroup = group;
        StateHasChanged();
    }

    private async Task OnGroupChange()
    {
        navManager.NavigateTo(navManager.Uri, true);
    }

    DeleteModal<Group> deleteModal;
    private void DeleteGroupClick()
    {
        deleteModal.ShowModal();
    }
    
    LeaveGroupModal leaveGroupModal;
    private async Task LeaveGroupClick()
    {
        await leaveGroupModal.ShowModal();
    }
    
    EditGroupModal editGroupModal;
    private async Task EditGroupClick()
    {
        await editGroupModal.ShowModal();
    }
    
    private async Task DeleteGroupCallback(Group group)
    {
        var groupId = group.GroupId!.Value;
        await _requestService.DeleteAsync($"/api/Group/{groupId}");
        await OnGroupChange();
    }
}
using Blazorise;

namespace MyFreezerBlazorApp.Pages;

public partial class Home
{
    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;
        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
        }
    }

}
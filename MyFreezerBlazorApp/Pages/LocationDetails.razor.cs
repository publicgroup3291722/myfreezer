using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Locations;
using MyFreezerBlazorApp.Components.Modals;
using MyFreezerBlazorApp.Components.Products;

namespace MyFreezerBlazorApp.Pages;

public partial class LocationDetails
{

    [Parameter]
    public string locationId { get; set; }

    public Location CurrentLocation { get; set; }

    Group parentGroup { get; set; } = new Group();

    CreateProductModal createProductModal;
    Product _tempProduct = new Product();
    private Place PlaceForCreation = new Place();

    private async Task CreateProductButtonClicked()
    {
        if (PlaceForCreation.PlaceId == null)
            throw new ArgumentNullException("PlaceId of PlaceForCreation is null");
        _tempProduct.PlaceId = PlaceForCreation.PlaceId;
        _tempProduct.Place = PlaceForCreation;
        await createProductModal.ShowModal();
    }
    
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        if (!int.TryParse(locationId, out int locationIdInt))
        {
            await NotificationService.Warning("Location id must be int!");
            navManager.NavigateTo("home");
            return;
        }
        var location = await _requestService.GetFromJsonAsync<Location>($"/api/Location/{locationIdInt}");

        if (location == null)
        {
            await NotificationService.Warning("Location not found!");
            navManager.NavigateTo("home");
            return;
        }
        CurrentLocation = location;
        parentGroup = location.Group;
        StateHasChanged();
    }

    EditLocationModal editLocationModal;
    private async Task EditLocationClick()
    {
        await editLocationModal.ShowModal();
    }

    DeleteModal<Location> deleteModal;
    private void DeleteLocationClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeleteLocationCallback(Location location)
    {
        await _requestService.DeleteAsync($"/api/Location/{location.LocationId}");
        await GoToParentGroup();
    }
    private async Task GoToParentGroup()
    {
        navManager.NavigateTo($"group/{parentGroup.GroupId.Value}");
        return;
    }
    private async Task GetLocation()
    {
        CurrentLocation = await _requestService.GetFromJsonAsync<Location>($"/api/location/{locationId}"); ;
        StateHasChanged();
    }
}
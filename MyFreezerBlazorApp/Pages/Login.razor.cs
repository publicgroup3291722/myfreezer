using Blazorise;
using Models.Models;
using Models.Props;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Pages;

public partial class Login
{
    public string LoginValue = null;

    public string Password = null;
    Alert errorAlert;
    string errorMessage;

    Alert successAlert;

    LoginField loginField;

    PasswordField passField;

    private bool IsFormValid()
    {
        if (!loginField.IsValid ||
            !passField.IsValid)
        {
            errorMessage = "Check form values";
            errorAlert.Toggle();
            return false;
        }
        return true;
    }

    private async void OnSubmit()
    {
        var isFormValid = IsFormValid();
        if (!isFormValid)
            return;
        LoginProp loginProp = new LoginProp
        {
            LoginOrEmail = LoginValue,
            Password = Password
        };

        try
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "api/Auth/login");
            JsonContent content = JsonContent.Create(loginProp, mediaType: null, null);
            request.Content = content;

            var responce = await _http.SendAsync(request);

            var accessToken = await responce.Content.ReadFromJsonAsync<JwtToken>();
            await cookieService.SetAccessToken(accessToken.Token);
            var tokenEntry = responce.Headers.AsEnumerable().Where(x => x.Key == "Set-Cookie").Select(x => x.Value).FirstOrDefault().FirstOrDefault();
            var tokenEqual = tokenEntry.Split(';').FirstOrDefault();
            var tokenValue = tokenEqual.Split('=').LastOrDefault();
            await cookieService.SetRefreshToken(tokenValue);
            navManager.NavigateTo("/home", true);
        }
        catch
        {
        }
    }

}
namespace MyFreezerBlazorApp.Pages;

public partial class Logout
{
    private async Task LogoutClick()
    {
        await cookieService.DeleteAccessToken();
        await cookieService.DeleteRefreshToken();
        navManager.NavigateTo("/", true);
    }

    private async Task CancelClick()
    {
        navManager.NavigateTo("/", true);
    }
}
using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;
using MyFreezerBlazorApp.Components.Places;

namespace MyFreezerBlazorApp.Pages;

public partial class PlaceDetails
{

    [Parameter]
    public string placeId { get; set; }

    public Place CurrentPlace { get; set; }

    Location parentLocation { get; set; } = new Location();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        var userId = await cookieService.GetUserId();


        if (!int.TryParse(placeId, out int placeIdInt))
        {
            await NotificationService.Warning("Place id must be int!");
            navManager.NavigateTo("home");
            return;
        }
        var place = await _requestService.GetFromJsonAsync<Place>($"/api/Place/{placeIdInt}");
        if (place == null)
        {
            await NotificationService.Warning("Place not found!");
            navManager.NavigateTo("home");
            return;
        }
        CurrentPlace = place;
        parentLocation = place.Location;
        StateHasChanged();
    }

    EditPlaceModal editPlaceModal;
    private async Task EditPlaceClick()
    {
        await editPlaceModal.ShowModal();
    }

    DeleteModal<Place> deleteModal;
    private void DeletePlaceClick()
    {
        deleteModal.ShowModal();
    }

    private async Task DeletePlaceCallback(Place place)
    {
        await _requestService.DeleteAsync($"/api/Place/{place.PlaceId}");
        await GoToParentLocation();
    }
    
    private async Task GoToParentLocation()
    {
        navManager.NavigateTo($"location/{parentLocation.LocationId}");
    }

}
using Models.Enums;
using Models.Models;
using Models.Options;
using MyFreezerBlazorApp.Components;
using MyFreezerBlazorApp.Components.ProductBases;

namespace MyFreezerBlazorApp.Pages;

public partial class ProductBases
{
    public int pageStart { get; set; } = 1;
    public int pageNum { get; set; } = 10;
    public int pageItems { get; set; }

    private ProductBaseOrderBy orderBy;
    private ProductBaseFilterBy filterBy;
    private string filterValue { get; set; }

    private List<ProductBase> _searchedProductBases { get; set; }

    ProductBase _tempProductBase { get; set; } = new ProductBase();

    CreateProductBaseModal createProductBaseModal;
    EditProductBaseModal editProductBaseModal;

    SelectPage selectPage;
    SelectPageNum selectPageNum;
    DeleteProductBaseModal deleteModal;

    private Task CreateProductBaseClick()
    {
        createProductBaseModal.ShowModal();
        return Task.CompletedTask;
    }
    
    private Task EditProductBaseClick()
    {
        editProductBaseModal.ProductBase = _tempProductBase;
        editProductBaseModal.ShowModal();
        return Task.CompletedTask;
    }
    
    private async Task DeleteProductBaseClick()
    {
        deleteModal.ShowModal();
    }
    
    private async Task OnProductBaseDelete(ProductBase productBase)
    {
        await LoadProductBases();
    }
    
    private async Task SearchClick()
    {
        await LoadProductBases();
    }
    
    private async Task LoadProductBases()
    {
        var options = new ProductBaseSortFilterPageOptions()
        {
            OrderBy = orderBy,
            FilterBy = filterBy,
            FilterValue = filterValue,
            PageNum = pageNum,
            PageStart = pageStart - 1
        };
        string query = await _requestService.GetRequestStringWithFormedOptions("api/ProductBase/getCountByOptions", options);
        int count = await _requestService.GetFromJsonAsync<int>(query);
        var baseValue = ((double)count / pageNum);
        pageItems = Convert.ToInt32(baseValue);
        if (baseValue - pageItems > 0)
            pageItems++;
        selectPage.MaxValue = pageItems;
        await selectPage.Refresh();

        var requestString = await _requestService.GetRequestStringWithFormedOptions("api/ProductBase", options);
        var productBases = await _requestService.GetFromJsonAsync<List<ProductBase>>(requestString);
        _searchedProductBases = productBases;
        StateHasChanged();
    }
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        await LoadProductBases();
    }
}
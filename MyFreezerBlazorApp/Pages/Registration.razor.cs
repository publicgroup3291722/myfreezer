using Blazorise;
using Models.DTOs;
using MyFreezerBlazorApp.Components.Form.TextFields;

namespace MyFreezerBlazorApp.Pages;

public partial class Registration
{

    public string Login = null;
    public string TelephoneNumber = null;
    public string Email = null;
    public string Password = null;
    public string RepeatPassword = null;

    Alert errorAlert;
    string errorMessage;

    Alert successAlert;

    LoginField loginField;
    EmailField emailField;
    TelephoneField telephoneField;
    PasswordField passField;
    PasswordField repeatPassField;

    private bool IsFormValid()
    {
        if (!loginField.IsValid ||
            !emailField.IsValid ||
            !telephoneField.IsValid ||
            !passField.IsValid ||
            !repeatPassField.IsValid)
        {
            errorMessage = "Check form values";
            errorAlert.Toggle();
            return false;
        }
        if (Password != RepeatPassword)
        {
            errorMessage = "Password is not match";
            errorAlert.Toggle();
        }
        return true;
    }

    private async void OnSubmit()
    {
        var isFormValid = IsFormValid();
        if (!isFormValid)
            return;
        UserDTO userDTO = new UserDTO
        {
            Login = Login,
            Password = Password,
            PasswordHash = Password,
            PhoneNumber = TelephoneNumber,
            Email = Email
        };
        try
        {
            await _httpClient.PostAsJsonAsync<UserDTO>("/api/Auth/register", userDTO);
            successAlert.Visible = true;
            navManager.NavigateTo("/login", true);
        }
        catch (Exception e)
        {
            errorMessage = $"Some error occured ({e.Message})";
            errorAlert.Visible = true;
        }
    }

}
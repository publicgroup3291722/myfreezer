using Microsoft.AspNetCore.Components;
using Models.Models;
using MyFreezerBlazorApp.Components.Modals;
using MyFreezerBlazorApp.Components.ShoppingLists;

namespace MyFreezerBlazorApp.Pages;

public partial class ShoppingListDetails
{

    [Parameter]
    public string shoppingListId { get; set; }

    public ShoppingList CurrentShoppingList { get; set; } = new ShoppingList();
    public ShoppingList _tempShoppingList { get; set; } = new ShoppingList();
    Group parentGroup { get; set; } = new Group();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userRole = await cookieService.GetUserRole();
        if (userRole == null)
        {
            navManager.NavigateTo("/");
            return;
        }

        if (!int.TryParse(shoppingListId, out int shoppingListIdInt))
        {
            await NotificationService.Warning("ShoppingList id must be int!");
            navManager.NavigateTo("home");
            return;
        }
        var shoppingList = await _requestService.GetFromJsonAsync<ShoppingList>($"/api/ShoppingList/{shoppingListIdInt}");

        if (shoppingList == null)
        {
            await NotificationService.Warning("ShoppingList not found!");
            navManager.NavigateTo("home");
            return;
        }
        CurrentShoppingList = shoppingList;
        parentGroup = shoppingList.Group;
        _tempShoppingList = CurrentShoppingList;
        StateHasChanged();
    }


    private async Task OnShoppingListChange()
    {
    }

    EditShoppingListModal editShoppingListModal;
    private async Task EditShoppingListClick()
    {
        _tempShoppingList = CurrentShoppingList;
        await editShoppingListModal.ShowModal();
    }

    DeleteModal<ShoppingList> deleteModal;
    private void DeleteShoppingListClick()
    {
        _tempShoppingList = CurrentShoppingList;
        deleteModal.ShowModal();
    }

    private async Task DeleteShoppingListCallback(ShoppingList shoppingList)
    {
        await _requestService.DeleteAsync($"/api/ShoppingList/{shoppingList.ShoppingListId}");
        await GoToParentGroup();
    }
    private async Task GoToParentGroup()
    {
        navManager.NavigateTo($"group/{parentGroup.GroupId.Value}");
        return;
    }
}
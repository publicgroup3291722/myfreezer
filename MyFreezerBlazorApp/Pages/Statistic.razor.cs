using Blazorise;
using Microsoft.AspNetCore.Components;
using Models.Enums;
using Models.Models;
using Models.Options;
using MyFreezerBlazorApp.Components;
using MyFreezerBlazorApp.Services;

namespace MyFreezerBlazorApp.Pages;

public partial class Statistic
{

    public int pageStart { get; set; } = 1;
    public int pageNum { get; set; } = 10;
    public int pageItems { get; set; }

    private ProductOrderBy orderBy;
    private ProductFilterBy filterBy;
    private string filterValue { get; set; }
    DateTime filterDate { get; set; } = DateTime.Now;
    SelectPage selectPage;
    SelectPageNum selectPageNum;

    List<Product> seachedProducts { get; set; }

    Select<int> selectGroupId { get; set; }
    Group SelectedGroup { get; set; }
    private List<Group> _groupsForPick { get; set; }

    private PathFormingService _pathFormingService = PathFormingService.Instance;
    
    protected async override Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
            return;

        var userId = await cookieService.GetUserId();
        _groupsForPick = await _requestService.GetFromJsonAsync<List<Group>>($"api/Group/getGroupsByUserId/{userId}");

        StateHasChanged();
        return;
    }


    private void GroupSelected(ChangeEventArgs e)
    {
        if (e.Value is not null)
        {
            int groupId = Convert.ToInt32(e.Value);
            if (groupId == 0)
                return;
            SelectedGroup = _groupsForPick.Where(group => group.GroupId == groupId).FirstOrDefault()!;
            StateHasChanged();
        }
    }
    
    private async Task SearchClick()
    {
        await LoadProductBases();
    }
    
    private async Task LoadProductBases()
    {
        var options = new ProductSortFilterPageOptions()
        {
            OrderBy = orderBy,
            FilterBy = ProductFilterBy.ByValidUntilBefore,
            FilterValue = Convert.ToString(filterDate),
            PageNum = pageNum,
            PageStart = pageStart - 1
        };
        string query = await _requestService.GetRequestStringWithFormedOptions("api/Product/getCountByOptions", options);
        int count = await _requestService.GetFromJsonAsync<int>(query);
        var baseValue = ((double)count / pageNum);
        pageItems = Convert.ToInt32(baseValue);
        if (baseValue - pageItems > 0)
            pageItems++;
        selectPage.MaxValue = pageItems;
        await selectPage.Refresh();

        var requestString = await _requestService.GetRequestStringWithFormedOptions("api/Product/", options);
        var productBases = await _requestService.GetFromJsonAsync<List<Product>>(
            requestString + "&groupId=" + SelectedGroup.GroupId.Value
        );
        seachedProducts = productBases;
        StateHasChanged();
    }

}
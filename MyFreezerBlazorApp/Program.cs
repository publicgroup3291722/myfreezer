﻿using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using MyFreezerBlazorApp.Services;

namespace MyFreezerBlazorApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor();
            builder.Services.AddHttpContextAccessor();

            builder.Services.AddScoped<CookieService>();
            builder.Services.AddScoped<RequestService>();

            builder.Services.AddScoped(hc =>
                new HttpClient()
                {
                    BaseAddress = new Uri("https://localhost:7126/api")
                }
                );

            builder.Services
                .AddBlazorise(options =>
                {
                    options.Immediate = true;
                    options.Debounce = true;
                    options.DebounceInterval = 300;
                })
                .AddBootstrapProviders()
                .AddFontAwesomeIcons();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            app.Run();
        }
    }
}
﻿using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using System.IdentityModel.Tokens.Jwt;

namespace MyFreezerBlazorApp.Services;

public class CookieService
{
    private ProtectedLocalStorage _storage;

    public CookieService(ProtectedLocalStorage storage)
    {
        _storage = storage;
    }

    public async Task<string> GetAccessToken()
    {
        try
        {
            var cookieValue = await _storage.GetAsync<string>("accessToken");
            return cookieValue.Value;
        }
        catch
        {
            await _storage.DeleteAsync("accessToken");
        }

        return null;
    }

    public async Task<string> GetRefreshToken()
    {
        try
        {
            var cookieValue = await _storage.GetAsync<string>("refreshToken");
            return cookieValue.Value;
        }
        catch
        {
            await _storage.DeleteAsync("refreshToken");
        }

        return null;
    }

    public async Task SetAccessToken(string token)
    {
        await _storage.SetAsync("accessToken", token);
    }

    public async Task SetRefreshToken(string token)
    {
        await _storage.SetAsync("refreshToken", token);
    }

    public async Task DeleteAccessToken()
    {
        await _storage.DeleteAsync("accessToken");
    }

    public async Task DeleteRefreshToken()
    {
        await _storage.DeleteAsync("refreshToken");
    }

    public async Task<int?> GetUserId()
    {
        string accessToken = await GetAccessToken();
        if (accessToken == null)
            return null;

        var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);

        var claims = jwt.Claims.ToList();
        var userIdStr = claims.Find(x => x.Type == "UserId");
        if (userIdStr == null)
            return null;
        return Convert.ToInt32(userIdStr.Value);
    }

    public async Task<string?> GetUserRole()
    {
        string accessToken = await GetAccessToken();
        if (accessToken == null)
            return null;

        var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);

        var claims = jwt.Claims.ToList();
        var role = claims.Find(x => x.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role");
        if (role == null) return null;
        return role.Value;
    }

    public async Task<string?> GetUserName()
    {
        string accessToken = await GetAccessToken();
        if (accessToken == null)
            return null;

        var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);

        var claims = jwt.Claims.ToList();
        var name = claims.Find(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name");
        if (name == null) return null;
        return name.Value;
    }
}
﻿using Models.Models;

namespace MyFreezerBlazorApp.Services;

public class PathFormingService
{
    private static PathFormingService _instance;
    public static PathFormingService Instance => _instance ??= new PathFormingService();
    public string GetPath(Place place)
    {
        return "/place/" + place.PlaceId;
    }
    
    public string GetPath(Location location)
    {
        return "/location/" + location.LocationId;
    }
}

﻿using Blazorise;
using Models.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Net;

namespace MyFreezerBlazorApp.Services;

public class RequestService
{
    private CookieService _cookiesService;
    private HttpClient _http;
    private readonly INotificationService _notificationService;

    public RequestService(CookieService cookiesService, HttpClient http, INotificationService notificationService)
    {
        _cookiesService = cookiesService;
        _http = http;
        _notificationService = notificationService;
    }

    private async Task CheckResponseForErrors(HttpResponseMessage response)
    {
        if (response.StatusCode > HttpStatusCode.IMUsed || response.StatusCode < HttpStatusCode.OK)
        {
            var error = await response.Content.ReadAsStringAsync();
            await _notificationService.Warning(error != "" ? error : response.ReasonPhrase);
        }
    }

    public async Task<HttpRequestMessage> GetRequestMessageAsync(HttpMethod requestMethod, string endpoint)
    {
        var accessToken = await _cookiesService.GetAccessToken();

        var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);

        if (jwt.ValidTo < DateTime.UtcNow)
        {
            var refreshRequest = new HttpRequestMessage(HttpMethod.Post, "api/Auth/refreshToken");
            var refreshTokenBefore = await _cookiesService.GetRefreshToken();
            refreshRequest.Headers.Add("refreshToken", refreshTokenBefore);
            var response = await _http.SendAsync(refreshRequest);

            var newAccessToken = await response.Content.ReadFromJsonAsync<JwtToken>();

            await _cookiesService.SetAccessToken(newAccessToken.Token);
            var tokenEntry = response.Headers.AsEnumerable().Where(x => x.Key == "Set-Cookie").Select(x => x.Value).FirstOrDefault().FirstOrDefault();
            var tokenEqual = tokenEntry.Split(';').FirstOrDefault();
            var tokenValue = tokenEqual.Split('=').LastOrDefault();
            await _cookiesService.SetRefreshToken(tokenValue);
            accessToken = newAccessToken.Token;
        }

        //var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);
        var request = new HttpRequestMessage(requestMethod, endpoint);
        request.Headers.Add("Authorization", "bearer " + accessToken);
        return request;
    }


    public async Task<HttpRequestMessage> GetRequestMessageAsync(
        HttpMethod requestMethod,
        string endpoint,
        FormUrlEncodedContent formData
        )
    {
        var request = await GetRequestMessageAsync(requestMethod, endpoint);
        request.Content = formData;
        return request;
    }


    public async Task<HttpRequestMessage> GetRequestMessageAsync(
        HttpMethod requestMethod,
        string endpoint,
        MultipartFormDataContent formData
    )
    {
        var request = await GetRequestMessageAsync(requestMethod, endpoint);
        request.Content = formData;
        return request;
    }


    public async Task<HttpResponseMessage> GetResponseAsync(HttpMethod requestMethod, string endpoint)
    {
        var request = await GetRequestMessageAsync(requestMethod, endpoint);
        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }


    public async Task<HttpResponseMessage> GetResponseAsync(
        HttpMethod requestMethod,
        string endpoint,
        FormUrlEncodedContent formData
        )
    {
        var request = await GetRequestMessageAsync(requestMethod, endpoint, formData);
        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }


    public async Task<HttpResponseMessage> GetResponseAsync(
        HttpMethod requestMethod,
        string endpoint,
        MultipartFormDataContent formData
    )
    {
        var request = await GetRequestMessageAsync(requestMethod, endpoint, formData);
        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }


    public async Task<HttpResponseMessage> GetResponseAsync<TValue>(HttpMethod requestMethod, string endpoint, TValue value)
    {
        var request = await GetRequestMessageAsync(requestMethod, endpoint);
        JsonContent content = JsonContent.Create(value, mediaType: null, null);
        request.Content = content;

        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }


    public async Task<TValue> GetFromJsonAsync<TValue>(string endpoint)
    {
        var request = await GetRequestMessageAsync(HttpMethod.Get, endpoint);
        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        var content = await response.Content.ReadFromJsonAsync<TValue>();
        return content;
    }


    public async Task<HttpResponseMessage> PostAsJsonAsync<TValue>(string endpoint, TValue value)
    {
        var request = await GetRequestMessageAsync(HttpMethod.Post, endpoint);
        JsonContent content = JsonContent.Create(value, mediaType: null, null);
        request.Content = content;
        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }

    public async Task<HttpResponseMessage> PutAsJsonAsync<TValue>(string endpoint, TValue? value)
    {
        var request = await GetRequestMessageAsync(HttpMethod.Put, endpoint);

        JsonContent content = JsonContent.Create(value, mediaType: null, null);
        request.Content = content;

        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }


    public async Task<HttpResponseMessage> PutAsync(string endpoint)
    {

        var request = await GetRequestMessageAsync(HttpMethod.Put, endpoint);
        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }


    public async Task<HttpResponseMessage> DeleteAsync(string endpoint)
    {
        var request = await GetRequestMessageAsync(HttpMethod.Delete, endpoint);
        var response = await _http.SendAsync(request);
        await CheckResponseForErrors(response);
        return response;
    }



    public async Task<string> GetRequestStringWithFormedOptions<TItem>(string endpoint, TItem options)
    {
        Type type = typeof(TItem);
        if (!type.Name.Contains("SortFilterPageOptions"))
            throw new ArgumentException("Given value is not SortFilterPageOptions");

        var orderByValue = type.GetProperty("OrderBy")!.GetValue(options);
        var filterByValue = type.GetProperty("FilterBy")!.GetValue(options);

        var filterValueProperty = type.GetProperty("FilterValue")!.GetValue(options) as string;
        string filterValue = filterValueProperty == null ? "" : filterValueProperty;

        var pageStart = type.GetProperty("PageStart")!.GetValue(options);
        var pageNum = type.GetProperty("PageNum")!.GetValue(options);

        string optionsQuery = $"?orderBy={orderByValue}&" +
            $"filterBy={filterByValue}&" +
            $"filterValue={filterValue}&" +
            $"pageNum={pageNum}&" +
            $"pageStart={pageStart}";

        return endpoint + optionsQuery;
    }
}

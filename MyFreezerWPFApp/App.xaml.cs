﻿using DataLayer.Data;
using DataLayer.Repositories.Implementations;
using DataLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MyFreezerBlazorApp.Services;
using MyFreezerWPFApp.Services;
using ServiceLayer.Services;
using System.Windows;

namespace MyFreezerWPFApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceProvider serviceProvider;
        public App()
        {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }
        private void ConfigureServices(ServiceCollection services)
        {
            services.AddSingleton<TokenService>();
            services.AddScoped<RequestService>();

            services.AddTransient<DataGenerator>();




            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IRefreshTokenRepository, RefreshTokenRepository>();
            services.AddTransient<IAuthRepository, AuthRepository>();

            services.AddScoped<UserService>();
            services.AddTransient<PasswordHasher>();

            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlServer("Server=localhost,3341;Database=MyFreezer;Trusted_Connection=False;TrustServerCertificate=True;User ID=sa;Password=SqlPass22;",
                    b => b.MigrationsAssembly("MyFreezerApp"));
            });

            services.AddSingleton<MainWindow>();
            services.AddSingleton<LoginOrRegister>();
            services.AddSingleton<Users>();
            services.AddTransient<CreateUser>();
        }
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var mainWindow = serviceProvider.GetRequiredService<MainWindow>();
            mainWindow.Show();
        }
    }

}

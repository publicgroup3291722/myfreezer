﻿using DataLayer.Repositories.Interfaces;
using Models.DTOs;
using Models.Models;
using System.Windows;

namespace MyFreezerWPFApp
{
    /// <summary>
    /// Interaction logic for CreateUser.xaml
    /// </summary>
    public partial class CreateUser : Window
    {
        public User SelectedUser { get; set; }
        private readonly IUserRepository _userRepository;
        private readonly IAuthRepository _authRepository;
        public CreateUser(IUserRepository userRepository, IAuthRepository authRepository)
        {
            _userRepository = userRepository;
            InitializeComponent();
            _authRepository = authRepository;
        }

        private void ClearVariables()
        {
            LoginTextBox.Text = string.Empty;
            PasswordTextBox.Text = string.Empty;
            EmailTextBox.Text = string.Empty;
            PhoneNumberTextBox.Text = string.Empty;
            IsAdminCheckBox.IsChecked = false;
        }

        private UserDTO FormUserDTO()
        {
            string password = PasswordTextBox.Text == "" ? null : PasswordTextBox.Text;
            int? userId = null;
            if (SelectedUser != null)
            {
                userId = SelectedUser.UserId;
                password = null;
            }
            var userDTO = new UserDTO
            {
                UserId = userId,
                Login = LoginTextBox.Text == "" ? null : LoginTextBox.Text,
                Email = EmailTextBox.Text == "" ? null : EmailTextBox.Text,
                PhoneNumber = PhoneNumberTextBox.Text == "" ? null : PhoneNumberTextBox.Text,
                IsAdmin = IsAdminCheckBox.IsChecked == true,
                Password = password,
                PasswordHash = password
            };
            return userDTO;
        }

        private async void CreateUserButton_Click(object sender, RoutedEventArgs e)
        {
            var userDTO = FormUserDTO();
            if (SelectedUser == null)
            {
                await _authRepository.RegisterUserAsync(userDTO);
                MessageBox.Show("User created!");
                ClearVariables();
                return;
            }
            SelectedUser.Login = LoginTextBox.Text == "" ? null : LoginTextBox.Text;
            SelectedUser.Email = EmailTextBox.Text == "" ? null : EmailTextBox.Text;
            SelectedUser.PhoneNumber = PhoneNumberTextBox.Text == "" ? null : PhoneNumberTextBox.Text;
            SelectedUser.IsAdmin = IsAdminCheckBox.IsChecked == true;
            await _userRepository.UpdateUserAsync(SelectedUser.UserId.Value, SelectedUser);
            MessageBox.Show("User changed!");
            this.Close();
        }

        private void Load(object sender, EventArgs e)
        {
            if (SelectedUser == null)
            {
                return;
            }
            LoginTextBox.Text = SelectedUser.Login;
            PasswordTextBox.Text = string.Empty;
            PhoneNumberTextBox.Text = SelectedUser.PhoneNumber;
            EmailTextBox.Text = SelectedUser.Email;
            IsAdminCheckBox.IsChecked = SelectedUser.IsAdmin;
            CreateUserButton.Content = "Edit user";
        }

    }
}

﻿using DataLayer.Repositories.Interfaces;
using MyFreezerWPFApp.Services;
using System.Windows;

namespace MyFreezerWPFApp
{
    /// <summary>
    /// Interaction logic for LoginOrRegister.xaml
    /// </summary>
    public partial class LoginOrRegister : Window
    {
        public bool IsAuthentificated = false;
        private readonly IAuthRepository _authRepository;
        private readonly TokenService _tokenService;
        public LoginOrRegister(IAuthRepository authRepository, TokenService tokenService)
        {
            InitializeComponent();
            _authRepository = authRepository;
            _tokenService = tokenService;
        }

        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            var login = LoginTextBox.Text;
            var password = LoginPasswordBox.Password;
            try
            {
                (var accessToken, var refreshToken) = await _authRepository.LoginAsync(new Models.Props.LoginProp { LoginOrEmail = login, Password = password });
                _tokenService.SetAccessToken(accessToken.Token);
                _tokenService.SetRefreshToken(refreshToken.Token);
                MessageBox.Show("You are logged in.", "Success");
                _tokenService.IsAuthenticated = true;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Problem occured");
                LoginTextBox.Text = string.Empty;
                LoginPasswordBox.Password = string.Empty;
            }

        }

        private async void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            var login = RegLoginTextBox.Text;
            var email = EmailTextBox.Text;
            var password = RegPasswordBox.Password;
            await _authRepository.RegisterUserAsync(new Models.DTOs.UserDTO { Login = login, Email = email, Password = password, PasswordHash = password });
            MessageBox.Show("User registered! Login now.");
            RegLoginTextBox.Text = string.Empty;
            EmailTextBox.Text = string.Empty;
            RegPasswordBox.Password = string.Empty;
        }



        private void OnLoginClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            IsAuthentificated = _tokenService.IsAuthenticated;
        }
    }
}

﻿using DataLayer.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System.Windows;

namespace MyFreezerWPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IAuthRepository _authRepository;
        private readonly IUserRepository _userRepository;
        private readonly IServiceProvider _serviceProvider;
        public MainWindow(IAuthRepository authRepository, IUserRepository userRepository, IServiceProvider serviceProvider)
        {
            _authRepository = authRepository;
            _userRepository = userRepository;
            _serviceProvider = serviceProvider;
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var users = await _userRepository.GetUserAsync(2026);
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            var log = _serviceProvider.GetRequiredService<LoginOrRegister>();
            log.ShowDialog();
            if (!log.IsAuthentificated)
            {
                this.Close();
            }
        }

        private void UsersButton_Click(object sender, RoutedEventArgs e)
        {
            var log = _serviceProvider.GetRequiredService<Users>();
            log.ShowDialog();
        }
    }
}
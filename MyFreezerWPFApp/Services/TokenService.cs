﻿using System.IdentityModel.Tokens.Jwt;

namespace MyFreezerWPFApp.Services;

public class TokenService
{
    public bool IsAuthenticated { get; set; }
    private string? _accessToken { get; set; }
    private string? _refreshToken { get; set; }
    public TokenService()
    {

    }

    public string GetAccessToken()
    {
        return _accessToken;
    }

    public string GetRefreshToken()
    {
        return _refreshToken;
    }

    public void SetAccessToken(string token)
    {
        _accessToken = token;
    }

    public void SetRefreshToken(string token)
    {
        _refreshToken = token;
    }

    public async Task DeleteAccessToken()
    {
        _accessToken = null;
    }

    public async Task DeleteRefreshToken()
    {
        _refreshToken = null;
    }

    public int? GetUserId()
    {
        string accessToken = GetAccessToken();
        if (accessToken == null)
            return null;

        var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);

        var claims = jwt.Claims.ToList();
        var userIdStr = claims.Find(x => x.Type == "UserId");
        if (userIdStr == null)
            return null;
        return Convert.ToInt32(userIdStr.Value);
    }

    public string GetUserRole()
    {
        string accessToken = GetAccessToken();
        if (accessToken == null)
            return null;

        var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);

        var claims = jwt.Claims.ToList();
        var role = claims.Find(x => x.Type == "Role");
        if (role == null) return null;
        return role.Value;
    }

    public string? GetUserName()
    {
        string accessToken = GetAccessToken();
        if (accessToken == null)
            return null;

        var jwt = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);

        var claims = jwt.Claims.ToList();
        var name = claims.Find(x => x.Type == "Name");
        if (name == null) return null;
        return name.Value;
    }
}

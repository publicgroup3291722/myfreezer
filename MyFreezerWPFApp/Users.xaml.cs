﻿using DataLayer.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Models.Models;
using System.Windows;

namespace MyFreezerWPFApp
{
    /// <summary>
    /// Interaction logic for Users.xaml
    /// </summary>
    public partial class Users : Window
    {
        public List<User> users;

        private readonly IUserRepository _userRepository;
        private readonly IServiceProvider _serviceProvider;
        public Users(IUserRepository userRepository, IServiceProvider serviceProvider)
        {
            _userRepository = userRepository;
            InitializeComponent();
            _serviceProvider = serviceProvider;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void GetUsersButton_Click(object sender, RoutedEventArgs e)
        {
            users = (await _userRepository.GetUsersAsync()).ToList();
            UsersDataGrid.ItemsSource = users;
        }

        private void CreateUserButton_Click(object sender, RoutedEventArgs e)
        {
            var createUserWindow = _serviceProvider.GetRequiredService<CreateUser>();
            createUserWindow.ShowDialog();
        }



        private void EditUserClick(object sender, RoutedEventArgs e)
        {
            var selectedUser = UsersDataGrid.SelectedItem as User;
            if (selectedUser != null)
            {
                var createUserWindow = _serviceProvider.GetRequiredService<CreateUser>();
                createUserWindow.SelectedUser = selectedUser;
                createUserWindow.ShowDialog();
            }
        }

        private async void DeleteUserClick(object sender, RoutedEventArgs e)
        {
            var selectedUser = UsersDataGrid.SelectedItem as User;
            await _userRepository.DeleteUserAsync(selectedUser);
            MessageBox.Show("User deleted.");
        }
    }
}

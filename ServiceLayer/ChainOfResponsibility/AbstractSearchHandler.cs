using Models.Models;
using Models.Options;

namespace ServiceLayer.ChainOfResponsibility;

public abstract class AbstractSearchHandler<TItem, TOptions> : ISearchHandler<TItem, TOptions>
{
    private ISearchHandler<TItem, TOptions> _nextHandler;

    public virtual ISearchHandler<TItem, TOptions> SetNext(ISearchHandler<TItem, TOptions> handler)
    {
        _nextHandler = handler;
        return handler;
    }

    public virtual IQueryable<TItem> Handle(IQueryable<TItem> queryCollection, TOptions options)
    {
        if (_nextHandler != null)
            return _nextHandler.Handle(queryCollection, options);

        return null;
    }
}
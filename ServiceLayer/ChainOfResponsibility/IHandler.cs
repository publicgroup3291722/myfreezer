namespace ServiceLayer.ChainOfResponsibility;

public interface ISearchHandler<TItem, TOptions>
{
    ISearchHandler<TItem, TOptions> SetNext(ISearchHandler<TItem, TOptions> handler);
    IQueryable<TItem> Handle(IQueryable<TItem> queryCollection, TOptions options);
}
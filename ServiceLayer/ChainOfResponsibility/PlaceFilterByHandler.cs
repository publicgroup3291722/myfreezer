using Models.DTOs;
using Models.Enums;
using Models.Models;
using Models.Options;

namespace ServiceLayer.ChainOfResponsibility;

public class PlaceFilterByHandler : AbstractSearchHandler<PlaceDTO, PlaceSortFilterPageOptions>
{
    private ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> _nextHandler;

    public override ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> SetNext(
        ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> handler)
    {
        _nextHandler = handler;
        return handler;
    }

    public override IQueryable<PlaceDTO> Handle(IQueryable<PlaceDTO> places,
        PlaceSortFilterPageOptions options)
    {
        if (places == null || options == null)
            _nextHandler.Handle(places, options);

        switch (options.FilterBy)
        {
            case PlaceFilterBy.NoFilter:
                break;
            case PlaceFilterBy.ByName:
                places = places.Where(x => x.Name.Contains(options.FilterValue));
                break;
            default:
                throw new ArgumentOutOfRangeException(
                    nameof(options.FilterBy), options.FilterBy, null);
        }

        if (_nextHandler != null)
            return _nextHandler.Handle(places, options);
        return places;
    }
}
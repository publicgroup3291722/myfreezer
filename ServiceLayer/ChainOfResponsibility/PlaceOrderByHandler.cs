using Models.DTOs;
using Models.Enums;
using Models.Models;
using Models.Options;

namespace ServiceLayer.ChainOfResponsibility;

public class PlaceOrderByHandler : AbstractSearchHandler<PlaceDTO, PlaceSortFilterPageOptions>
{
    private ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> _nextHandler;

    public override ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> SetNext(
        ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> handler)
    {
        _nextHandler = handler;
        return handler;
    }

    public override IQueryable<PlaceDTO> Handle(IQueryable<PlaceDTO> places,
        PlaceSortFilterPageOptions options)
    {
        if (places == null || options == null)
            _nextHandler.Handle(places, options);

        switch (options.OrderBy)
        {
            case PlaceOrderBy.ByPlaceIdDESC:
                places = places.OrderByDescending(place => place.PlaceId);
                break;
            case PlaceOrderBy.ByNameDESC:
                places = places.OrderByDescending(place => place.Name);
                break;
            case PlaceOrderBy.ByPlaceIdASC:
                places = places.OrderBy(place => place.PlaceId);
                break;
            case PlaceOrderBy.ByNameASC:
                places = places.OrderBy(place => place.Name);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(options.OrderBy), options.OrderBy, null);
        }

        if (_nextHandler != null)
            return _nextHandler.Handle(places, options);
        return places;
    }
}
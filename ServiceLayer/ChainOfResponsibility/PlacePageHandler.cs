using Models.DTOs;
using Models.Models;
using Models.Options;

namespace ServiceLayer.ChainOfResponsibility;

public class PlacePageHandler : AbstractSearchHandler<PlaceDTO, PlaceSortFilterPageOptions>
{
    private ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> _nextHandler;

    public override ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> SetNext(
        ISearchHandler<PlaceDTO, PlaceSortFilterPageOptions> handler)
    {
        _nextHandler = handler;
        return handler;
    }

    public override IQueryable<PlaceDTO> Handle(IQueryable<PlaceDTO> places, PlaceSortFilterPageOptions options)
    {
        if (options.PageNum <= 0)
            throw new ArgumentOutOfRangeException(
                nameof(options.PageNum), "pageSize cannot be zero.");

        if (options.PageStart >= 0)
        {
            places = places.Skip(options.PageStart * options.PageNum);
        }

        places = places.Take(options.PageNum);

        if (_nextHandler != null)
            return _nextHandler.Handle(places, options);
        return places;
    }
}